$html_bienvenue_template = @(END)

<html><head>
<title>Bienvenue sur le site du server <%= $hostname %> !</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Bienvenue sur le site du server <%= $hostname %> </h1>
<p>Ceci est une page d'exemple servant a montrer le potentiel
du templating avec le gestionnaire de configuration Puppet.</p>
<p><em>Thank you.</em></p>
</body></html>

END

node 'puppet-node1', 'puppet-node2' {
  package { 'cowsay':
    ensure => absent,
  }
  package { 'nginx-core':
    ensure => absent,
  }
  
  file { '/var/www/html/index.nginx-debian.html':
    content => inline_epp($html_bienvenue_template),
    ensure => absent,
  }

  # include apache

  # apache::vhost { 'cat-pictures.com':
  #   port => '8080',
  #   docroot => '/var/www/cat-pictures',
  #   docroot_owner => 'www-data',
  #   docroot_group => 'www-data',
  # }

  # file { '/var/www/cat-pictures/index.html':
  #   content => "<img src='http://bitfieldconsulting.com/files/happycat.jpg'>",
  #   owner => 'www-data',
  #   group => 'www-data',
  # }

  class { 'wordpress':
  }

  apache::vhost { 'puppet-wordpress':
    port => '8080',
    docroot => '/var/www',
    docroot_owner => 'www-data',
    docroot_group => 'www-data',
  }

}
